Meta:
@regUsers

Narrative:
As a user
I want to be able to change profile picture
So that I can personalize my profile

Scenario: Navigate to home page and set profile picture
Given Tab profile is present
When Click profile tab
And Tab profileInfo is present
And Click profileInfo tab
And Button changeProfilePicture is present
When Click changeProfilePicture button
And Button browse is present
Then I upload a profile picture
Meta:
@publicPart

Narrative:
As a non-registered user
I want to make a registration

Scenario:
Given Element newUserButton is present
And Click newUserButton element
When Element haveAnAccountButton is present
And Element socialNetworkLogo is present
And Click firstNameField element
And Type firstName in firstNameField field
And Click lastNameField element
And Type lastName in lastNameField field
And Click emailRegistrationField element
And Type email in emailRegistrationField field
And Click passwordField element
And Type password in passwordField field
And Click confirmPasswordField element
And Type password in confirmPasswordField field
When Click signUpButton element
And Element ageField is present
And Type age in ageField field
When Click createProfileButton element
Then Element postField is present

Meta:
@publicPart

Narrative:
As a non-registered user
I want to see the profile information (only First and Last Name must be visible)

Scenario:
Given Tab searchField is present
When Click firstUser element
And Element postField is not present
Then Click profileInfo element
When Element publicUserFirstName is present
And Element publicUserAge is not present
Then Element publicUserLastName is present
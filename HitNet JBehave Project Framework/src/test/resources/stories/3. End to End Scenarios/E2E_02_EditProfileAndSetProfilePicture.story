Meta:
@e2e

Narrative:
As a user
I want to edit my profile (to change my name and age)
To change my profile picture

Scenario:
Given I have logged in
When Edit my profile
And Element postField is present
And Click profileAdministrationButton button
And Click profileInfo button
And Click changeProfilePicture element
Then I upload a profile picture
package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import java.io.File;

public class UserActions {
	public static WebDriver driver;

	public UserActions() {
		driver = Utils.getWebDriver();
	}

	private static Screen screen = new Screen();

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("hitnet.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	public static void clickElement(String key){
		Utils.LOG.info("Clicking on element " + key);
		waitForElementVisible(key, 30);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
	}

	public static void typeValueInField(String value, String field){
		Utils.LOG.info("Typing: " + value + " in " + field);
		waitForElementVisible(field, 10);
		clearField(field);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(value);
	}

	public static void clearField(String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(Keys.CONTROL+"a");
		element.sendKeys(Keys.BACK_SPACE);
	}

	//############# WAITS #########

	public static void waitForElementVisible(String locator, int seconds){
		WebElement element= driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	//############# ASSERTS #########

	public static void assertElementPresent(String locator){
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
	}
	public void assertElementNotPresent(String locator) {
		ExpectedConditions.invisibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator)));
	}


	//############# REGISTERED USERS #########

	public static void navigateTo(String locator){
		waitForElementVisible(locator, 30);
		assertElementPresent(locator);
		clickElement(locator);
		Utils.LOG.info("Navigated to field");
    }


	public static void uploadPhoto() {
		File profilePic = new File("src/test/resources/Images/profile_picture.png");
		String absolutePath = profilePic.getAbsolutePath();
		driver.findElement(By.xpath("//input[contains(@id,'profile-pic-upload-btn')]")).sendKeys(absolutePath);
		clickElement("saveChanges");
	}

	public static void logOut() throws FindFailed {
		String optionsButton = "src/test/resources/Images/optionsButton.PNG";
		screen.wait(optionsButton, 10);
		screen.click(optionsButton);
		clickElement("logOutButton");
		Utils.LOG.info("LogOut button clicked");
	}
}
